package com.web.utilities;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {

	public static String[][] getDataValues(String dataFileName, String sheetName, String testName) throws IOException {
		try {
			FileInputStream fs = new FileInputStream(dataFileName);
			XSSFWorkbook workbook = new XSSFWorkbook(fs);
			String[][] dataTable = null;
			XSSFSheet sheet = workbook.getSheet(sheetName);
			int totalRows = sheet.getLastRowNum();
			int totalColumns = sheet.getRow(0).getLastCellNum();

			for (int i = 0; i < totalColumns; i++) {
				XSSFRow rows = sheet.getRow(0);
				XSSFCell cell = rows.getCell(i);
				dataTable[0][i] = cell.getStringCellValue();
			}
			for (int i = 1; i <= totalRows; i++) {
				XSSFRow rows = sheet.getRow(i);
				XSSFCell cell = rows.getCell(0);
				if ((cell.getStringCellValue().equals(testName))) {
					for (int j = 0; j < totalColumns; j++) {
						cell = rows.getCell(j);
						if (cell == null) {
							dataTable[i][j] = null;
						} else {
							if (cell.getCellType() == cell.CELL_TYPE_STRING) {
								dataTable[i][j] = cell.getStringCellValue();
							} else if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
								String cellText = String.valueOf(cell.getNumericCellValue());
								dataTable[i][j] = cellText;
							} else {
								dataTable[i][j] = String.valueOf(cell.getBooleanCellValue());
								
							}
						}
					}
					break;
				}
			}
			//workbook.close();
			return dataTable;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

}