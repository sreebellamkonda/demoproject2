package com.web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.web.base.Resuable;

public class AmazonHomepage extends Resuable {

	public WebDriver driver = null;

	public AmazonHomepage(WebDriver driver) {
		super(driver);
		this.driver = driver;

	}

	By Locator = By.name("q");
	By selectamazon = By.xpath("//h3[contains(text(),'Amazon.in')]");
	By Signin = By.xpath("//span[contains(text(),'Sign in')]");

	
	///comments
	public void performSearch() {
		elementToBePresent(Locator);
		WebElement SearchBox = driver.findElement(Locator);
		insertText(SearchBox, "amazon", 5);
		keysReturn(SearchBox, 30);
	}

	public void clickOnAmazon() {
		// elementToBePresent(Locator1);
		WebElement amazon = driver.findElement(selectamazon);
		performClick(amazon);
	}

	public void clickOnSignin() {
		WebElement clickSigninbutton = driver.findElement(Signin);
		performClick(clickSigninbutton);
	}


}
