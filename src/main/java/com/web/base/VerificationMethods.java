package com.web.base;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
//import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class VerificationMethods {
WebDriver driver;	
ExtentTest report;

public VerificationMethods(WebDriver driver) {
	this.driver=driver;
}

public VerificationMethods(WebDriver driver,ExtentTest report){  //constructor with input parameters for webdriver and extentreports
	this.report = report;
	this.driver = driver;
}

	//swicth to frame verfication method
public void validatePresenceofFrame(){
	List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
	if(iframes.size() > 0) {	
	report.log(LogStatus.PASS, iframes.size() +  " are present ");
	Assert.assertTrue(true, iframes.size() +  " are  present ");
	} else{
		report.log(LogStatus.FAIL, iframes.size() +  " are not present ");		
		Assert.assertFalse(false, iframes.size() +  " are not present ");		 
		}	
}

public void validatePresenceofWindowHandle(){
	Set<String> allWindowHandles = driver.getWindowHandles();
	int count = allWindowHandles.size();
if(count > 0) {	
	report.log(LogStatus.PASS, count +  " windows are present ");
	Assert.assertTrue(true, count +  " windows are present ");
	} else{
		report.log(LogStatus.FAIL,count +  " are not present ");		
		Assert.assertFalse(false, count +  " are not present ");		 
		}	
}


public void validateTwoString(String actual,String expected){
if(actual.equalsIgnoreCase(expected)) {	
	report.log(LogStatus.PASS, actual +  " matching with "+expected);
	Assert.assertEquals(actual, expected);
	} else{
		report.log(LogStatus.FAIL, actual +  " not matching with "+expected);		
	Assert.assertEquals(actual, expected);
	}	
}


public void validateLabelofWebElement(WebElement element,String expected) {
	
String actual=element.getText();
validateTwoString(actual,expected);	
	
}


public boolean validateDisplayofWebElement(WebElement element) {
	 // boolean submitbuttonPresence=element.isDisplayed();
	  if(element.isDisplayed()) {
		  report.log(LogStatus.PASS, element + "  is displayed");
		Assert.assertTrue(element.isDisplayed(), "element is displayed");		 
		  return true; 
	  }else {
		  report.log(LogStatus.FAIL, element + "  is not displayed");
		  Assert.assertFalse(element.isDisplayed(), "element is not displayed");		 
			
		return false;  
	  }
      
	
}

public boolean validateEnablementofWebElement(WebElement element) {
	  if(element.isEnabled()) {
		  report.log(LogStatus.PASS, element + "  is enabled");
		Assert.assertTrue(element.isEnabled(), "element is enabled");		 
		  return true; 
	  }else {
		  report.log(LogStatus.FAIL, element + "  is not enabled");
		  Assert.assertFalse(element.isEnabled(), "element is not enabled"); 
		return false;  
	  }
	
}

public boolean validateselectionofWebElement(WebElement element) {
	 if(element.isSelected()) {
		  report.log(LogStatus.PASS, element + "  is selected");
		Assert.assertTrue(element.isSelected(), "element is selected");		 
		  return true; 
	  }else {
		  report.log(LogStatus.FAIL, element + "  is not selected");
		  Assert.assertFalse(element.isSelected(), "element is not selected"); 
		return false;  
	  }
}

public void validateListofLabelString(List<WebElement> elements,List<String> expectedList) {
if(elements.size()==expectedList.size()) {
for(int i=0;i<elements.size();i++ )	{
	validateLabelofWebElement(elements.get(i),expectedList.get(i));	
}
}else {
	  report.log(LogStatus.FAIL, "elements size is "+ elements.size() +"  not matching with expected list " + expectedList.size());
		
	 Assert.assertFalse(false, "elements size is "+ elements.size() +"  not matching with expected list " + expectedList.size());		 
		
}
}

/*public void captureScreenshot() throws IOException {
	 // Store the screenshot in current project dir.
    String screenShot = System.getProperty("user.dir")+"\\Artifacts\\FileName.png";

    // Call Webdriver to click the screenshot.
    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    String destination = System.getProperty("user.dir") + "/screenshots/" +  getTimeStamp()+ ".png";
  
        FileUtils.copyFile(scrFile, new File(destination));
    }*/


public String getTimeStamp() {
	return new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(Calendar.getInstance().getTime()).trim();
}

}
