package com.web.base;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Service {
	
public WebDriver driver = null;	
public static String getresourcefile(String resourceName)

{
	URI uri;
	File file = null;
	try
	{
		uri = ClassLoader.getSystemResource(resourceName).toURI();
	 file= new File(uri);
	}
	
	catch(URISyntaxException e)
	{
	
	
}
return file.getAbsolutePath();

}
public void setDriverPath()

{
System.setProperty("webdriver.gecko.driver","E:\\eclipse-workspace\\DemoProject\\resources\\Drivers\\geckodriver.exe");
	
	System.setProperty("webdriver.chrome.driver","E:\\eclipse-workspace\\DemoProject\\resources\\Drivers\\chromedriver.exe");
	
	System.setProperty("webdriver.ie.driver","E:\\eclipse-workspace\\DemoProject\\resources\\Drivers\\IEDriverServer.exe");
	
	System.setProperty("webdriver.edge.driver","E:\\eclipse-workspace\\DemoProject\\resources\\Drivers\\msedgedriver.exe") ;
}
public WebDriver initializeDriver(String browserName)


{
	setDriverPath();
	switch(browserName)
	{
	
	case "chrome": 
	{
		
		driver = new ChromeDriver();
		
		break;
	}
	
		case "IE":
		{
			
			driver = new InternetExplorerDriver();
			
			break;
		}
			
       case "gecko": 
       {
			
			driver = new FirefoxDriver();
			
			break;
	}
              
       case "Edge":
       {
			
			driver = new EdgeDriver();
			
			break;
       }
       default : 
       {
    	   driver = new ChromeDriver();
       }
       }
	return driver;
}

}

