package com.web.tests;

import java.io.IOException;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.web.base.Service;
import com.web.pages.AmazonAddtocart;
import com.web.pages.AmazonHomepage;
import com.web.pages.AmazonSearchpage;
import com.web.pages.AmazonSelectProduct;
import com.web.pages.Page1;
import com.web.pages.Signinpage;
import com.web.pages.Signout;
import com.web.utilities.DataProvider1;

public class Test1 {
	
	public WebDriver driver;	
	
	static ExtentTest test;
	static ExtentReports report;
	
	
	@BeforeTest	
	public void beforeTest(){
	driver=new Service().initializeDriver("chrome");	
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	}
	
	@BeforeClass
	public static void startTest()
	{
	report = new ExtentReports(System.getProperty("user.dir")+"\\ExtentReportResults.html");
	test = report.startTest("ExtentDemo");
	}
	
	
	/*	public static String dataFileNAme=DataMappingconstants.TestDatapath+DataMappingConstants.TestDataFile;
		public static void setSheetName(String psheetName)
		{
			sheetName=psheetName;
			data
	*/
	
	
	@Test(priority = 1)
	public void test() throws InterruptedException, IOException{
	
	Page1 one=new Page1(this.driver);
	AmazonHomepage amazon=new AmazonHomepage(this.driver);
	Signinpage signin=new Signinpage(this.driver);
	AmazonSearchpage searchpage = new AmazonSearchpage(driver);
	AmazonSelectProduct selectproduct= new AmazonSelectProduct(driver);
	AmazonAddtocart addcart =new AmazonAddtocart(this.driver);
	Signout signout = new Signout(this.driver);
	
	driver.get("https://www.google.com");
	test.log(LogStatus.PASS, "Navigated to the specified URL");
	
	amazon.performSearch();
	test.log(LogStatus.PASS, "perform  search");
	
	amazon.clickOnAmazon();
	test.log(LogStatus.PASS, "click on amazon");
	
	amazon.clickOnSignin();	
	test.log(LogStatus.PASS, "click on sigin button");
	
	signin.enterEmailAddress();
	test.log(LogStatus.PASS, "enter email address");
	
	signin.clickOnContinue();
	test.log(LogStatus.PASS, "click on continue button");
	
	signin.enterPassword();
	test.log(LogStatus.PASS, "enter password");
	
	signin.clickOnSigin();
	test.log(LogStatus.PASS, "click on sign in button");
	
	searchpage.searchProduct();
	test.log(LogStatus.PASS, "search for apple watch");
		
	selectproduct.selectProduct();
	test.log(LogStatus.PASS, "select  apple watch");
	
	addcart.addtoCart();
	test.log(LogStatus.PASS, "click on add to cart");
	
	addcart.clickOnCancel();
	test.log(LogStatus.PASS, "click on cancel");	
	
	addcart.clickOnSigin();
	test.log(LogStatus.PASS, "click on signin link");
	 Thread.sleep(3000);
	signout.clickOnSigout();
	test.log(LogStatus.PASS, "click on signout link");
	
	}
	
	/*@Test (priority = 2)
	public void test2() throws InterruptedException, IOException{
	Page1 one=new Page1(this.driver);
	AmazonHomepage amazon=new AmazonHomepage(this.driver);
	Signinpage signin=new Signinpage(this.driver);
	AmazonSearchpage searchpage = new AmazonSearchpage(driver);
	AmazonSelectProduct selectproduct= new AmazonSelectProduct(driver);
	AmazonAddtocart addcart =new AmazonAddtocart(this.driver);
		
	driver.get("https://www.google.com");
	
	amazon.performSearch();
	amazon.clickOnAmazon();	
	searchpage.searchProduct();
	
	selectproduct.selectProduct();
	
	addcart.addtoCart();
	
	addcart.clickOnCancel();
	addcart.clickOnSigin();
	
   // amazon.clickOnSignin();
	
	signin.enterEmailAddress();
	signin.clickOnContinue();
	signin.enterPassword();
	signin.clickOnSigin();

	}*/
	
	@AfterTest	
	public void terminateBrowser(){
	driver.close();
	driver.quit();
	}
	
	@AfterClass
	public static void endTest()
	{
	report.endTest(test);
	report.flush();
	}
}
